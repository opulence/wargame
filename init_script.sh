#!/bin/sh

USER="admin"
USER_HOME="/home/$USER"
HOSTNAME=`hostname`
EMAIL_ADDRESS="$HOSTNAME@opulence.fr"

# Create user "admin" with "admin" group
adduser --system --group --shell /bin/bash --disabled-password $USER

# Copy ssh public from user vagrant
cp -pr /home/vagrant/.ssh $USER_HOME
chown -R "$USER:$USER" $USER_HOME

# Disable root and vagrant accounts
usermod --shell /sbin/nologin vagrant
usermod --shell /sbin/nologin root

# Update system
#apt-get update


# Create OpenPGP key configuration file
cat > key.conf << EOF
%echo Création des clefs OpenPGP
Key-Type: RSA
Key-Length: 2048
Subkey-Type: RSA
Subkey-Length: 2048

Name-Real: $HOSTNAME
Name-Email: $EMAIL_ADDRESS
Name-Comment: Generation des flags de $HOSTNAME
Expire-Date: 0

%no-ask-passphrase
%no-protection
%commit
%echo Terminé!
EOF

# Generate OpenPGP keys (flag-keys)
((find / | xargs file) > /dev/null 2>&1  &) ; gpg --verbose --batch --gen-key key.conf

# Export public key to admin's home directory
file="$USER_HOME/$EMAIL_ADDRESS.pub"
rm -f $file
gpg --armor --output $file --export $EMAIL_ADDRESS

rsync -e  "ssh -o StrictHostKeyChecking=no"  $file flagkeys@51.38.179.48:/srv/public_keys/$EMAIL_ADDRESS