

# wargame


# Authentification sur le serveur de jeu

### Architecture


                                  +--------------------------+
                                  |     Nom de domaine:      |
                                  |     IP: 51.38.179.48     |
                                  +------------+-------------+
                                               |                         
                                               |                         
                 +-----------------------------+-----------------------------+
                 |                             |                             |
                 |                             |                             |
                 |                             |                             |
                 |                             |                             |
      +----------v------------+    +-----------v-----------+    +------------v----------+
      | Nom de domaine: louis |    |  Nom de domaine: gus  |    | Nom de domaine: henry |
      | IP: 192.168.0.10      |    |  IP: 192.168.0.11     |    | IP: 192.168.0.12      |
      +-----------------------+    +-----------------------+    +-----------------------+


### Serveur de jeu

Addresse IP: **51.38.179.48**
port SSH: **22**


Utilisateurs:

| User | password | Definition|
| ------ | ------ |-----------------------------|
| flagkeys | ZDkxN2RiYWUzZWM3ZTg0NTM2NmM5NzRl | Compte utilisé pour sauvegarder les clef publiques permettant la génération des flags |
| louis | -- | Compte perso de Louis |
| gus | ZTNiNDEyMzFhOGM3OWUwMWMxNjNjNjFj | Compte perso de Augustin |
| henry | YTgyNDAyZDAxOTdlMGIxMzU0YmNjODla | Compte perso de Henry |


### Etapes par étapes

**1.  Se connecter sur le serveur de jeu**

Envois de votre clef publique vers le serveur en tant que `authorized_key` pour votre compte perso (remplacez `louis` par votre user)

```
ssh-copy-id louis@51.38.179.48
```


Envois de votre clef publique vers le serveur en tant que `authorized_key`  pour le compte commun (flagkeys)
```
ssh-copy-id flagkeys@51.38.179.48
```


Vous devriez maintenant pouvoir vous connecter sur le serveur de jeu.
```
ssh louis@51.38.179.48
```

Vous pouvez changez votre mot de passe 
```
passwd louis
```

> Note: vos comptes utilisateurs sur le serveur de jeu ne sont pas sécurisés (je suis root sur le serveur ...).
> Veillez donc à pas laisser traîner de choses sensible sur ce serveur.

**2.  Ajout de votre clef publique**

L'ajout de clef publique permet de créer vos machines.

Assurez vous d'avoir sur votre machine votre clef publique dans ~/.ssh/id_rsa.pub, sinon vous pouvez changer le chemin.
```
ssh louis@51.38.179.48 "mkdir -p ~/.ssh"
rsync -r ~/.ssh/id_rsa.pub louis@51.38.179.48:~/.ssh/id_rsa.pub
```

Une fois que chaque utilisateur (louis, gus, henry) sont bien connectés sur le serveur de jeu, et que les clefs publiques sont dans les dossiers `~/.ssh/id_rsa.pub`, nous pouvons installer les machines.



**3.  Installation des machines**

Les machines sont installées via un fichier de config vagrant et un script bash.
Les fichiers sont disponibles sur le serveur de jeu dans `/srv/machines`.

Pour créer les machines il suffit d'exécuter les commandes suivantes
Supprimer les machines:
```
cd /srv/machines && vagrant destroy
```
Créer les machines
```
cd /srv/machines && vagrant up
```

**4.  Se connecter aux machines**

Il est maintenant possible de vous connecter à vos machines de deux manières:

**Se connecter depuis le serveur de jeu:**
```
ssh -A louis@51.38.179.48
```
Il est obligatoire de fournir l'argument `-A` qui permet de forward votre clef privée temporairement sur le serveur de jeu.
```
ssh admin@louis
```

**Se connecter via un tunnel SSH:**

```
ssh -J louis@51.38.179.48 admin@louis
```

> Les machines sont sécurisés. (sauf preuve du contraire ...) Il est impossible de s'y connecter sans détenir votre clef privée SSH. Même en tant que `root`.